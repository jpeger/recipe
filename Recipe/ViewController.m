//
//  ViewController.m
//  Recipe
//
//  Created by Stephen Bromley on 1/21/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray* imageNames = [[NSArray alloc]initWithObjects:@"sevenSauce.png", @"garlicChicken.png", @"meatloaf.png", @"coqAuVin.png", @"salsaChicken.png", nil];
    NSArray* recipeNames = [[NSArray alloc]initWithObjects:@"Seven Ingredient Tomato Sauce",@"Garlic Chicken Stir Fry", @"Classic Meatloaf", @"Chef John's Coq Au Vin", @"Salsa Chicken", nil];
    NSArray* recipes = [[NSArray alloc]initWithObjects:@"2 tablespoons olive oil        7 cloves garlic, minced     1 (6 ounce) can tomato paste        1 (28 ounce) can crushed tomatoes       2 (28 ounce) cans tomato puree      1/2 teaspoon ground black pepper        1/2 teaspoon salt       2 teaspoons dried basil leaves      1 teaspoon white sugar      Directions      1 Heat the olive oil in a large saucepan and cook and stir garlic, being careful not to burn it. Pour in tomato paste and simmer on low for 5 minutes. Add crushed tomatoes, tomato puree, pepper, salt, basil and sugar; stir. Cook on low for three hours, stirring occasionally.", @"2 tablespoons peanut oil      6 cloves garlic, minced     1 teaspoon grated fresh ginger      1 bunch green onions, chopped       1 teaspoon salt     1 pound boneless skinless chicken breasts, cut into strips      2 onions, thinly sliced     1 cup sliced cabbage        1 red bell pepper, thinly sliced        2 cups sugar snap peas      1 cup chicken broth     2 tablespoons soy sauce     2 tablespoons white sugar       2 tablespoons cornstarch        Directions     1 Heat peanut oil in a wok or large skillet. When oil begins to smoke, quickly stir in 2 cloves minced garlic, ginger root, green onions and salt. Stir fry until onion becomes translucent, about 2 minutes. Add chicken and stir until opaque, about 3 minutes. Add remaining 4 cloves minced garlic and stir. Add sweet onions, cabbage, bell pepper, peas and 1/2 cup of the broth/water and cover.      2 In a small bowl, mix the remaining 1/2 cup broth/water, soy sauce, sugar and cornstarch. Add sauce mixture to wok/skillet and stir until chicken and vegetables are coated with the thickened sauce. Serve immediately, over hot rice if desired", @"1 carrot, coarsely chopped       1 rib celery, coarsely chopped      1/2 onion, coarsely chopped         1/2 red bell pepper, coarsely chopped       4 white mushrooms, coarsely chopped         3 cloves garlic, coarsely chopped       2 1/2 pounds ground chuck       1 tablespoon Worcestershire sauce       1 egg, beaten                               1 teaspoon dried Italian herbs      2 teaspoons salt          1 teaspoon ground black pepper        1/2 teaspoon cayenne pepper         1 cup plain bread crumbs        1 teaspoon olive oil                        Glaze Ingredients:      2 tablespoons brown sugar       2 tablespoons ketchup       2 tablespoons Dijon mustard         hot pepper sauce to taste   Directions   1 Preheat the oven to 325 degrees F.       2 Place the carrot, celery, onion, red bell pepper, mushrooms, and garlic in a food processor, and pulse until very finely chopped, almost to a puree. Place the minced vegetables into a large mixing bowl, and mix in ground chuck, Worcestershire sauce, and egg. Add Italian herbs, salt, black pepper, and cayenne pepper. Mix gently with a wooden spoon to incorporate vegetables and egg into the meat. Pour in bread crumbs. With your hand, gently mix in the crumbs with your fingertips just until combined, about 1 minute.        3 Form the meatloaf into a ball. Pour olive oil into a baking dish and place the ball of meat into the dish. Shape the ball into a loaf, about 4 inches high by 6 inches long.      4 Bake in the preheated oven just until the meatloaf is hot, about 15 minutes.      5 Meanwhile, in a small bowl, mix together brown sugar, ketchup, Dijon mustard, and hot sauce. Stir until the brown sugar has dissolved.        6 Remove the meatloaf from the oven. With the back of a spoon, smooth the glaze onto the top of the meatloaf, then pull a little bit of glaze down the sides of the meatloaf with the back of the spoon.        7 Return meatloaf to oven, and bake until the loaf is no longer pink inside and the glaze has baked onto the loaf, 30 to 35 more minutes. An instant-read thermometer inserted into the thickest part of the loaf should read at least 160 degrees F (70 degrees C).", @"6 bone-in, skin-on chicken thighs     1 pinch kosher salt and freshly ground black pepper to taste        8 ounces bacon, sliced crosswise into 1/2-inch pieces       10 large button mushrooms, quartered        1/2 large yellow onion, diced       2 shallots, sliced          2 teaspoons all-purpose flour       2 teaspoons butter      1 1/2 cups red wine         6 sprigs fresh thyme        1 cup chicken broth     Directions      1 Preheat oven to 375 degrees F (190 degrees C).        2 Season chicken thighs all over with salt and black pepper.        3 Place bacon in a large, oven-proof skillet and cook over medium-high heat, turning occasionally, until evenly browned, about 10 minutes. Transfer bacon with a slotted spoon to a paper-towel lined plate, leaving drippings in the skillet.      4 Increase heat to high and place chicken, skin-side down, into skillet. Cook in hot skillet until browned, 2 to 4 minutes per side. Transfer chicken to a plate; drain and discard all but 1 tablespoon drippings from the skillet.        5 Lower heat to medium-high; saute mushrooms, onion, and shallots with a pinch of salt in the hot skillet until golden and caramelized, 7 to 12 minutes.        6 Stir flour and butter into vegetable mixture until completely incorporated, about 1 minute.       7 Pour red wine into the skillet and bring to a boil while scraping browned bits of food off of the bottom of the pan with a wooden spoon. Stir bacon and thyme into red wine mixture; simmer until wine is about 1/3 reduced, 3 to 5 minutes. Pour chicken broth into wine mixture and set chicken thighs into skillet; bring wine and stock to a simmer.      8 Cook chicken in the preheated oven for 30 minutes. Spoon pan juices over the chicken and continue cooking until no longer pink at the bone and the juices run clear, about 30 minutes more. An instant-read thermometer inserted into the thickest part of the thigh, near the bone should read 165 degrees F (74 degrees C). Transfer chicken to a platter.      9 Place skillet over high heat and reduce pan juices, skimming fat off the top as necessary, until sauce thickens slightly, about 5 minutes. Season with salt and pepper; remove and discard thyme. Pour sauce over chicken.", @"4 skinless boneless chicken breast halves      4 teaspoons taco seasoning mix      1 cup salsa         1 cup shredded Cheddar cheese       2 tablespoons sour cream (optional)         Directions          1. Preheat oven to 375 degrees F (190 degrees C)     2. Place chicken breasts in a lightly greased 9x13 inch baking dish. Sprinkle taco seasoning on both sides of chicken breasts, and pour salsa over all.        3. Bake at 375 degrees F (190 degrees C) for 25 to 35 minutes, or until chicken is tender and juicy and its juices run clear.       4. Sprinkle chicken evenly with cheese, and continue baking for an additional 3 to 5 minutes, or until cheese is melted and bubbly. Top with sour cream if desired, and serve", nil];
    
    UIScrollView* mainView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [mainView setBackgroundColor:[UIColor orangeColor]];
    [mainView setPagingEnabled:YES];
    [mainView setContentSize:CGSizeMake(self.view.frame.size.width*5, self.view.frame.size.height)];
    [self.view addSubview:mainView];
    
    for (int x = 0; x<5; x++) {
        UIScrollView* view = [[UIScrollView alloc]initWithFrame:CGRectMake(x*self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
        UIImageView* pic = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[imageNames objectAtIndex:x]]];
        UILabel* name;
        UITextView* recip;
        
        if (x!=2) {
            name = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, self.view.frame.size.width-20, 100)];
            [pic setFrame:CGRectMake(name.frame.origin.x, name.frame.origin.y + name.frame.size.height, name.frame.size.width, 100)];
            recip = [[UITextView alloc]initWithFrame:CGRectMake(pic.frame.origin.x, pic.frame.origin.y+pic.frame.size.height+50, pic.frame.size.width, self.view.frame.size.height-200)];
        } else {
            [pic setFrame:CGRectMake(10, 50, self.view.frame.size.width-20, 100)];
            name = [[UILabel alloc]initWithFrame:CGRectMake(pic.frame.origin.x, pic.frame.origin.y + pic.frame.size.height, pic.frame.size.width , 100)];
            recip = [[UITextView alloc]initWithFrame:CGRectMake(name.frame.origin.x, name.frame.origin.y+name.frame.size.height, name.frame.size.width, self.view.frame.size.height-200)];

        }
        [name setText:[recipeNames objectAtIndex:x]];
        [name setTextAlignment:NSTextAlignmentCenter];
        [recip setText:[recipes objectAtIndex:x]];
        [pic setContentMode:UIViewContentModeScaleAspectFit];
        [recip setUserInteractionEnabled:NO];
        [recip sizeToFit];
        [view addSubview:name];
        [view addSubview:pic];
        [view addSubview:recip];
        [view setContentSize:CGSizeMake(mainView.frame.size.width,mainView.frame.size.height*3)];
        [mainView addSubview:view];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
